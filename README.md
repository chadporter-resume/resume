<h1 align="center">
  Chad Porter Resume
</h1>

<p align="center">
  Resume in LaTeX format
</p>

<div align="center">
  <a href="https://chadporter.net/resume.pdf">
    <img alt="Chad Porter Resume" src="https://img.shields.io/badge/resume-pdf-green.svg" />
  </a>
  <a href="https://chadporter.net/coverletter.pdf">
    <img alt="Chad Porter Cover Letter" src="https://img.shields.io/badge/coverletter-pdf-green.svg" />
  </a>
</div>

<br />

**Resume** and **Cover Letter** are in LaTeX format inspired by [Fancy CV](https://www.sharelatex.com/templates/cv-or-resume/fancy-cv).

#### Requirements

A full TeX distribution. [Install TeX Live from upstream](http://tex.stackexchange.com/q/1092).

#### Usage
```bash
$ xelatex {resume}.tex
```

This should output ``{resume}.pdf``

## Credit

[**LaTeX**](http://www.latex-project.org) is a typesetting program with academic roots in math and computer science.

[**LaTeX FontAwesome**](https://github.com/furl/latex-fontawesome) bindings for FontAwesome icons to be used in XeLaTeX.

[**Lato**](https://github.com/google/fonts/tree/master/ofl/lato) a sans serif typeface family with semi-rounded details of the letters giving it a feeling of warmth, while the strong structure provides stability and seriousness.

[**Source Sans Pro**](https://github.com/adobe-fonts/source-sans-pro) is a set of OpenType fonts that have been designed to work well in user interface (UI) environments.


## Contact

You are free to take my `.tex` file and modify it to create your own resume.

## See Also

* [Chad Porter Resume Website](https://gitlab.com/chadporter/resume-website) - A single-page Hugo website.
